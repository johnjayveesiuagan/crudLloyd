<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyCRUDController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::resource('companies', CompanyCRUDController::class);

Route::group([
    'as' => "companies.",
    // 'middleware' => "auth"
], function () {

    Route::get(
        '/companies',
        ['uses' => 'CompanyCRUDController@index', 'as' => 'index']
    );

    Route::get(
        '/companies/create',
        ['uses' => 'CompanyCRUDController@create', 'as' => 'create']
    );

    Route::get(
        '/companies/edit/{id}',
        ['uses' => 'CompanyCRUDController@edit', 'as' => 'edit']
    );

    Route::post(
        '/companies/store/',
        ['uses' => 'CompanyCRUDController@store', 'as' => 'store']
    );

    Route::PUT(
        '/companies/update/{id}',
        ['uses' => 'CompanyCRUDController@update', 'as' => 'update']
    );

    Route::delete(
        '/companies/{id}',
        ['uses' => 'CompanyCRUDController@destroy', 'as' => 'destroy']
    );
});
